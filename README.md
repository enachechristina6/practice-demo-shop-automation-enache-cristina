# Demo Shop Test Framework

## Technology
 - Java 19
 - TestNg
 - Allure reports
 - Google Chrome browser
 - IntelliJ Idea Community Edition
 - Apache Maven
 - Java, Allure, Selenide, CSS
 - GitLab
 - Data Provider

### Description
Demo Shop automation is a project that testing a demo shop application.
This application applied "Single Responsibility Principle ",used best practices  and all tests are writed clean and simple. 
    