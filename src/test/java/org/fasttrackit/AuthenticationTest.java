package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Test(groups = "AuthenticationTest")
public class AuthenticationTest extends Config {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modalDialog = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToResetButton();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Description("User turtle can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Cristina Enache")
    @Lead("Maria Enache")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS-001")
    @Story("Login with valid credentials")
    @TmsLink("https://fasttrackit-test.netlify.app/#/issues/DMS-001")
    @Story("Login with valid credential.")

    public void valid_user_can_login_with_valid_credentials(ValidAccount account) {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername(account.getUser());
        modalDialog.typeInPassword(account.getPassword());
        modalDialog.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Logged in with valid user, expected greetings message to be:" + account.getGreetingsMsg());
    }

    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class)
    public void non_valid_logins_with_invalid_credentials(InvalidAccount account) {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername(account.getUser());
        modalDialog.typeInPassword(account.getPassword());
        modalDialog.clickOnTheLoginButton();
        boolean errorMessageVisible = modalDialog.isErrorMessageVisible();
        String errorMsg = modalDialog.getErrorMsg();
        assertTrue(errorMessageVisible, "error is visible.");
        assertEquals(errorMsg, account.getErrorMsg());
    }



}
