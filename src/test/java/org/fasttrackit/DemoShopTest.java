package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Epic("Login")
@Severity(SeverityLevel.CRITICAL)
@Feature("User can Login to DemoShop App.")

public class DemoShopTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToResetButton();
    }

    @Test(description = "User turtle can login with valid credentials.")
    @Description("User turtle can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Cristina Enache")
    @Lead("Maria Enache")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS-001")
    @Story("Login with valid Credentials")
    public void user_turtle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greetings message to be Hi turtle!");
    }

    @Test
    public void user_dino_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "Logged in with turtle, expected greetings message to be Hi turtle!");
    }

    @Test
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page.");
    }

    @Test
    public void user_can_navigate_to_Cart_Page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on Cart page.");
    }

    @Test
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page.");
    }

    @Test(dependsOnMethods = "user_can_navigate_to_Cart_Page")
    public void user_can_navigate_to_Home_Page_from_Cart_Page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on Cart page.");
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page.");
    }

    @Test
    public void user_can_add_product_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1. ");
    }

    @Test
    public void user_can_add_two_product_to_cart_from_product_cards() {
        ProductCards cards = new ProductCards();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart, badge shows 2. ");
    }

}