package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

import static org.fasttrackit.dataprovider.ProductsDataProvider.*;

public class UserAndProductsDataProvider {

    @DataProvider(name = "userAndProduct")
    public Object[][] getUsersAndProduct() {
        return new Object[][]{
                {AuthenticationDataProvider.beetle, agc},
                {AuthenticationDataProvider.beetle, ich},
                {AuthenticationDataProvider.turtle, agc},
                {AuthenticationDataProvider.dino, agc},
        };
    }
    @DataProvider(name = "usersAndProducts")
    public Object[][] getUsersAndProducts () {
        List<ProductData> products = new ArrayList<>();
        products.add(agc);
        products.add(ich);
        return new Object[][]{
                {AuthenticationDataProvider.beetle, products},
                {AuthenticationDataProvider.beetle, products},
                {AuthenticationDataProvider.turtle, products},
                {AuthenticationDataProvider.dino, products}
        };
    }
}