package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
@Ignore
public class ProductSortingTest {
    Page page = new Page();
    ProductCards productlist = new ProductCards();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToResetButton();
    }

    @Test
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically() {
        Product firstProductBeforeSort = productlist.getFirstProductInList();
        Product lastProductBeforeSort = productlist.getLastProductInList();
        productlist.clickOnTheSortButton();
        productlist.clickOnTheAZSortButton();
        Product firstProductAfterSort = productlist.getFirstProductInList();
        Product lastProductAfterSort = productlist.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
    }

    @Test
    public void when_sorting_products_z_to_a_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productlist.getFirstProductInList();
        Product lastProductBeforeSort = productlist.getLastProductInList();
        productlist.clickOnTheSortButton();
        productlist.clickOnTheZASortButton();
        Product firstProductAfterSort = productlist.getFirstProductInList();
        Product lastProductAfterSort = productlist.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productlist.clickOnTheSortButton();
        productlist.clickOnTheZASortButton();

        Product firstProductBeforeSort = productlist.getFirstProductInList();
        Product lastProductBeforeSort = productlist.getLastProductInList();
        productlist.clickOnTheSortButton();
        productlist.clickOnTheZASortButton();
        Product firstProductAfterSort = productlist.getFirstProductInList();
        Product lastProductAfterSort = productlist.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductAfterSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductAfterSort.getPrice());


    }
    @Test
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productlist.clickOnTheSortButton();
        productlist.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productlist.getFirstProductInList();
        Product lastProductBeforeSort = productlist.getLastProductInList();
        productlist.clickOnTheSortButton();
        productlist.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productlist.getFirstProductInList();
        Product lastProductAfterSort = productlist.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
}
}

