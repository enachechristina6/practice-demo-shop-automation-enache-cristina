package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
public class WishlistTest {
    Page page = new Page();
    Header header = new Header();
    @BeforeClass
    public void setup() {
        page.openHomepage();
    }
    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToResetButton();
    }

    @Test
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page.");
    }

}