package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import org.fasttrackit.dataprovider.UserAndProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class UsersAndProductsInteractionTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modalDialog = new ModalDialog();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToResetButton();
    }
    @Link(name = "FastTrackIt Demo Shop",url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS-001")
    @Test(dataProviderClass = UserAndProductsDataProvider.class, dataProvider = "userAndProduct")
    public void logged_in_user_can_add_a_product_to_cart(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername(account.getUser());
        modalDialog.typeInPassword(account.getPassword());
        modalDialog.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());

    }
    @Test(dataProviderClass = UserAndProductsDataProvider.class, dataProvider = "usersAndProducts")
    public void logged_in_user_can_add_multiple_products_to_cart(ValidAccount account, List<ProductData> productsData) {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername(account.getUser());
        modalDialog.typeInPassword(account.getPassword());
        modalDialog.clickOnTheLoginButton();
//        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertTrue(cartPage.isEmptyCartMessageDisplayed());
    }
}
