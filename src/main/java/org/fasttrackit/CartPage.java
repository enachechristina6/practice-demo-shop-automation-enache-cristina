package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private SelenideElement emptyCartPageElement = $(".text-center");
    private String emptyCartPage;

    public CartPage() {
    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }
    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }
}
