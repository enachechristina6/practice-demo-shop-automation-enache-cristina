package org.fasttrackit;

import com.codeborne.selenide.AssertionMode;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println("- = DemoShop = -");
        System.out.println("1.User can log with valid credentials.");
        Page page = new Page();
        page.openHomepage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi Dino is displayed in the header:" + isLogged);
        System.out.println("Greetings msg is: " + greetingsMessage);

        System.out.println("---------------------");
        System.out.println("2.User can add product to cart from product cards.");
        page.openHomepage();
        ProductCards cards = new ProductCards();
//        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
//        System.out.println("Product is: " + awesomeGraniteChips);
//        awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println("---------------------");
        System.out.println("3. User can navigate to Home Page from Wishlist Page.");
        page.openHomepage();
        header.clickOnTheWishListIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on Wishlist page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on Home page. Title is:" + pageTitle);

        System.out.println("---------------------");
        System.out.println("4. User can navigate to Home Page from Cart Page");
        page.openHomepage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Cart page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on Home page. Title is:" + pageTitle);
    }
}

// -User can navigate to Home page from Wishlist Page
// 1. Open Home Page.
// 2. Click on the "favorites icon".
// 3. Click on the "Shopping bag" icon.
// 4. Expected results: Home Page is Loaded.

//    - User can add product to cart from product cards
//1. Open page.
//2. Click on the "Awesome Granite Chips" cart icon.
// Expected results: Mini Cart icon shows 1 product in cart.


//1. Open Homepage.
//2. Click on the Login button.
//3. Click on the Username Field.
//4. Type in Dyno.
//5. Click on the password field.
//6. Type in choochoo.
//7. Click on the Login button.
// Expected: Hi Dino is displayed in the header.