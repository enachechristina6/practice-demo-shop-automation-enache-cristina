package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");

    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");

    @Step("Click on the Login Button.")
    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login Button.");
    }
    public String getGreetingsMessage() {
        return greetingElement.text();
    }
    public void clickOnTheWishListIcon() {
        System.out.println("Click on the wishlist button");
        wishlistButton.click();
    }
    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }
    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }
    public boolean isShopingBadgeVisible() {
        return  !this.shoppingCartBadges.isEmpty();

    }
}


